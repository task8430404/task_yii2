<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Advanced TASK Project Template</h1>
    <br>
</p>

-------------------
git clone
```
git clone https://gitlab.com/task8430404/task_yii2.git
```
Go to the project directory
```
cd task_yii2
```
Run docker containers
```
docker compose up -d
```
Install composer scripts:
```
docker compose exec php composer install
```
yii migrate
```
docker compose exec php  php yii migrate
```
Done! You can open http://localhost:8087/ via browser. By the way, you can change this port by changing

`DOCKER_NGINX_PORT` variable in .env file.

# Docker

For enter to php container run `docker compose exec php bash`

For enter to mysql container run `docker compose exec mysql bash`

For enter to nginx container run `docker compose exec nginx bash`

You can change containers prefix by changing `DOCKER_PROJECT_NAME` variable in .env file.