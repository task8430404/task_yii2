<?php

namespace frontend\controllers;

use common\models\Person;
use common\models\Traits\UploadFileSave;
use frontend\models\UploadFrom;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class PersonController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['profile', 'update', 'upload'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['profile', 'update', 'upload'],
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionProfile(): Response|string
    {
        return $this->render('profile', [
            'model' => $this->findModelIdentity(),
        ]);
    }


    /**
     * @throws NotFoundHttpException
     */
    public function actionUpload(): Response|string
    {
        $model = $this->findModelIdentity();
        $uploadForm = new UploadFrom;

        if ($uploadForm->load(Yii::$app->request->post())) {
            $modelUpload = new UploadFileSave($uploadForm);
            $modelUpload->saveData($model);
            return $this->redirect(['profile']);
        }

        return $this->render('upload', [
            'model' => $model,
            'uploadForm' => $uploadForm
        ]);
    }

    /**
     * Updates an existing Person model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(): Response|string
    {
        $model = $this->findModelIdentity();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['profile']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the Person model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Person the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): Person
    {
        if (($model = Person::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * @throws NotFoundHttpException
     */
    protected function findModelIdentity(): Person|Response|null
    {

       $identityUserId = Yii::$app->user->identity->getId();

        if (($model = Person::findOne(['user_id' => $identityUserId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}