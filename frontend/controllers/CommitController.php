<?php

namespace frontend\controllers;

use common\models\Commit;
use common\models\Person;
use common\models\search\CommitSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CommitController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
            return [
                'access' => [
                    'class' => AccessControl::class,
                    'only' => ['index', 'create'],
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['index', 'create'],
                            'roles' => ['@'],
                        ]
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'logout' => ['post'],
                    ],
                ],
            ];
    }

    /**
     * Lists all commit models.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new CommitSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate(): Response|string
    {
        $model = new Commit();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->save(false);
                return $this->redirect(['site/index']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelIdentity($id);

        if ($model){
            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }else{
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionDelete($id): Response
    {
        $model = $this->findModelIdentity($id);

        if ($model){
            $model->isDeleted();
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id): ?Commit
    {
        if (($model = Commit::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * @throws NotFoundHttpException
     */
    protected function findModelIdentity($id): Commit|Response|bool
    {
        $model = Commit::findOne(['id' => $id]);

        if ($model->created_by !== Yii::$app->user->identity->getId()) {
            return false;
        }

        return $this->findModel($id);
    }

}