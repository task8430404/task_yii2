<?php

namespace frontend\models;

use yii\base\Model;

class UploadFrom extends Model
{
    public $photoFile;

    public function rules(): array
    {
        return [
            [['photoFile'], 'file', 'maxFiles' => 20, 'skipOnEmpty' => false, 'extensions' => 'svg,png,jpg,jpg,jpeg'],
        ];
    }
}