<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var common\models\Commit $model */

$this->params['breadcrumbs'][] = ['label' => 'Commits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="commit-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <p class="card-text"> <?= Html::encode($model->description) ?></p>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
