<?php
/* @var common\models\Commit $model */
use yii\helpers\Url;
?>

<?php foreach ($model as $value): ?>
    <div class="card mb-2">
        <div class="card-header d-flex justify-content-between">
            <div>
                <a href="<?= Url::to(['commit/view', 'id' => $value->id]) ?>"><?= $value->created->person->last_name ?></a>
            </div>
            <div>
                <?= $value->date;  ?>
            </div>
        </div>
        <div class="card-body">
            <p class="card-text"> <?= $value->description ?></p>
        </div>
    </div>

<?php endforeach; ?>
