<?php
use yii\bootstrap5\LinkPager;
/* @var yii\data\Pagination $pagination */
?>
<nav aria-label="Page navigation example" class="d-flex justify-content-left mt-3">
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
        'class'=>'border-warning'
    ]);
    ?>
</nav>
