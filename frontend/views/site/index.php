<?php

/** @var yii\web\View $this */
/* @var yii\data\Pagination $pagination */
/* @var common\models\Commit $commit */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>Comments</h2>
                <?= $this->render('commit',['model' => $commit]) ?>
                <?= $this->render('pagination',['pagination' => $pagination]) ?>
            </div>
        </div>
    </div>
</div>
