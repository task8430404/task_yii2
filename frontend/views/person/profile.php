<?php

use common\Helpers\PersonHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var common\models\Person $model */

$this->params['breadcrumbs'][] = ['label' => 'People'];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="person-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Avatar upload file', ['upload'], ['class' => 'btn btn-secondary']) ?>
        <?= Html::a('Update', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'avatar',
                'value' => $model->getPhotoSrc(),
                'format' => ['image', ['width' => 140, 'height' => 80]]
            ],

            'last_name',
            'first_name',
            'birthday',
            'phone',
            [
                'attribute' => 'gender',
                'value' => function ($model) {
                    return PersonHelper::getGenderLabel($model->gender);
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return PersonHelper::getStatusLabel($model->status);
                },
                'format' => 'html'
            ],

        ],
    ]) ?>

</div>
