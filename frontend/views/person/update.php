<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Person $model */

$this->title = 'Update Person';
$this->params['breadcrumbs'][] = ['label' => 'People', 'url' => ['profile']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="person-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
