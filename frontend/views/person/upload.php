<?php

use frontend\models\UploadFrom;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var UploadFrom $uploadForm */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="person-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($uploadForm, 'photoFile')->fileInput() ?>
    <br>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
