<?php

namespace common\ExceptionConstants;

class ExceptionConstants
{
    public const ERROR_MESSAGE = 'Произошла ошибка при сохранении данных.';
    public const SUCCESS_MESSAGE = 'Данные созданы успешно';
    public const ERR_MESSAGE = 'Произошла ошибка. Пожалуйста, попробуйте еще раз';

    public const registration = 'Thank you for registration. Please check your inbox for verification email.';
    public const ERR_KEY = 'error';
    public const SUCCESS_KEY = 'success';
}