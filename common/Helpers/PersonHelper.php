<?php

declare(strict_types=1);

namespace common\Helpers;

use Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class PersonHelper
{
    public static function getGenderList(): array
    {
        return [
            Constants::GENDER_MALE => 'male',
            Constants::GENDER_FEMALE => 'female',
        ];
    }

    public static function getStatusList(): array
    {
        return [
            Constants::STATUS_ACTIVE => 'active',
            Constants::STATUS_INACTIVE => 'inactive',
        ];
    }

    /**
     * @throws Exception
     */
    public static function getGenderLabel($type): string
    {
        return ArrayHelper::getValue(self::getGenderList(), $type);
    }


    /**
     * @throws Exception
     */
    public static function getStatusLabel($status): string
    {
        $class = match ($status) {
            Constants::STATUS_ACTIVE => 'text-success',
            Constants::STATUS_INACTIVE => 'text-danger',
            default => 'text-default',
        };

        return Html::tag('span', ArrayHelper::getValue(self::getStatusList(), $status), [
            'class' => $class,
        ]);
    }
}