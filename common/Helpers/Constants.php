<?php
namespace common\Helpers;
class Constants
{
    public const GENDER_MALE = 1;
    public const GENDER_FEMALE = 0;

    public const STATUS_ACTIVE = 1;
    public const STATUS_INACTIVE = 0;
}