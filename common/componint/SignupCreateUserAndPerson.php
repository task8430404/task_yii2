<?php

declare(strict_types=1);

namespace common\componint;

use common\ExceptionConstants\ExceptionConstants;
use common\models\Person;
use common\models\User;
use Exception;
use frontend\models\SignupForm;
use Yii;

class SignupCreateUserAndPerson
{
    private SignupForm|null $form;

    /**
     * @throws \yii\db\Exception
     */
    public function userAndPersonSave(SignupForm $form): bool|null|Exception
    {
        $this->form = $form;

        $user = $this->userFactory();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $user->save(false) ?? throw new Exception(ExceptionConstants::ERROR_MESSAGE);
            $this->personFactory($user->id)->save(false) ?? throw new Exception(ExceptionConstants::ERROR_MESSAGE);
            Yii::$app->session->setFlash(ExceptionConstants::SUCCESS_KEY, ExceptionConstants::SUCCESS_MESSAGE);
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            Yii::$app->session->setFlash(ExceptionConstants::ERR_KEY, ExceptionConstants::ERR_MESSAGE . $e->getMessage());
            $transaction->rollBack();
            throw $e;
        }
    }

    private function userFactory(): User
    {
        return (new UserFactory())->create(
            $this->form->getUsername(),
            $this->form->getEmail(),
            $this->form->getPassword()
        );
    }


    private function personFactory(int $userId): Person
    {
        return (new PersonFactory())->create(
            $this->form->getLastName(),
            $this->form->getFirstName(),
            $this->form->getBirthday(),
            $this->form->getPhone(),
            $this->form->getGender(),
            $userId,
        );

    }
}