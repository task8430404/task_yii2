<?php
declare(strict_types=1);

namespace common\componint;

use common\models\User;

class UserFactory
{
    public function create(string $username, string $email, string $password): User
    {
        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        return $user;
    }
}