<?php
declare(strict_types=1);

namespace common\componint;

use common\models\Person;

class PersonFactory
{
    public function create(
        string       $lastName,
        string       $firstName,
        string       $birthday,
        string       $phone,
        int          $gender,
        int          $userId,
    ): Person
    {
        $person = new Person();
        $person->last_name = $lastName;
        $person->first_name = $firstName;
        $person->birthday = $birthday;
        $person->phone = $phone;
        $person->gender = $gender;
        $person->user_id = $userId;

        return $person;
    }
}