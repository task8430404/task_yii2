<?php

namespace common\models;

use common\models\Traits\TraitUploadFile;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "person".
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $birthday
 * @property string|null $phone
 * @property int|null $gender
 * @property string|null $avatar
 * @property int $user_id
 * @property int|null $status
 * @property int|null $is_deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property User $user
 */
class Person extends ActiveRecord
{

    use TraitUploadFile;

    public $photoFile;
    const PATH_PHOTO = '/uploads/photos/avatar';
    const PATH_FILE = 'avatar_';
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'person';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['last_name', 'first_name', 'birthday', 'user_id', 'created_at', 'updated_at'], 'required'],
            [['gender', 'user_id', 'status', 'is_deleted', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['last_name', 'first_name', 'phone'], 'string', 'max' => 30],
            [['birthday'], 'string', 'max' => 10],
            [['avatar'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['photoFile'], 'file', 'maxFiles' => 20, 'skipOnEmpty' => true, 'extensions' => 'svg, png,jpg,jpg,jpeg,jfif,pdf,mdf,'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'last_name' => 'Last Name',
            'first_name' => 'First Name',
            'birthday' => 'Birthday',
            'phone' => 'Phone',
            'gender' => 'Gender',
            'avatar' => 'Avatar',
            'user_id' => 'User ID',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
