<?php

namespace common\models\query;

use Yii;

/**
 * This is the ActiveQuery class for [[\common\models\commit]].
 *
 * @see \common\models\Commit
 */
class CommitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Commit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Commit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function sort_desc(): CommitQuery
    {
        return $this->orderBy(['id' => SORT_DESC]);
    }

    public function noDeleted(): CommitQuery
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

    public function identity(): CommitQuery
    {
        return $this->andWhere(['created_by' => Yii::$app->user->identity->getId()]);
    }
}
