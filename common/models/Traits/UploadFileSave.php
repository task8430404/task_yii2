<?php

namespace common\models\Traits;

use common\models\Person;
use frontend\models\UploadFrom;
use yii\web\UploadedFile;

readonly class UploadFileSave
{
    public function __construct(private UploadFrom $from)
    {
    }

    public function saveData(Person $model): bool
    {
        $model->photoFile = UploadedFile::getInstance($this->from, 'photoFile');

        switch ($model->avatar){
            case null:
                $model->uploadPhoto();
                break;
            case !null:
                $model->updatePhoto();
                break;
        }

        return $model->save(false);
    }
}