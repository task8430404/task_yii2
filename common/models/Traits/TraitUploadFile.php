<?php

namespace common\models\Traits;

use Yii;
use yii\helpers\Url;

trait TraitUploadFile
{

    public static function getLastId(): int
    {
        $lastId = self::find()->select('id')->orderBy(['id' => SORT_DESC])->scalar() ?? 0;
        return ++$lastId;
    }


    public static function getPhotoAlias(): string
    {
        return Yii::getAlias('@appRoot') . self::PATH_PHOTO;
    }


    public function getPhotoSrc(): string
    {
        return Url::to(self::PATH_PHOTO . '/' . $this->avatar);
    }

    public function isPhotoExists(): bool
    {
        return file_exists(self::getPhotoAlias() . '/' . $this->avatar);
    }


    public function deletePhoto(): bool
    {
        return unlink(self::getPhotoAlias() . '/' . $this->avatar);
    }


    public function generatePhotoName(): string
    {
        return self::PATH_FILE . self::getLastId() . '-' . (int)(microtime(true) * (1000)) . '.' . $this->photoFile->extension;
    }


    public function getSavePhoto(string $photoName): void
    {
        $this->photoFile->saveAs(self::getPhotoAlias() . '/' . $photoName);
    }


    public function fileCreated(): void
    {
        if (!file_exists(self::getPhotoAlias())) {
            mkdir(self::getPhotoAlias(), 0777, true);
        }
    }


    public function imageDb(string $photoName): void
    {
        $this->avatar = $photoName;
    }


    public function updatePhoto(): void
    {
        if ($this->isPhotoExists()) {
            $this->deletePhoto();
        }
        $this->uploadPhoto();
    }

    public function uploadPhoto(): bool
    {
        if (!$this->validate(false)) {
            return false;
        }

        $photoName = $this->generatePhotoName();
        $this->fileCreated();
        $this->getSavePhoto($photoName);
        $this->imageDb($photoName);
        return true;
    }


    public function deleteUpdatePhoto($image): bool
    {
        return unlink(self::getPhotoAlias() . '/' . $image);
    }

}