<?php

namespace common\models;

use common\models\query\CommitQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "commit".
 *
 * @property int $id
 * @property string $description
 * @property int|null $status
 * @property int|null $is_deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property User $user
 */
class Commit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commit';
    }


    public function behaviors(): array
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['description', 'created_at', 'updated_at'], 'required'],
            [['description'], 'string'],
            [['status', 'is_deleted', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getPages(): array
    {
        $pagination = $this->getPagination();

        return $this->getQueryCommit()->offset($pagination->offset)
            ->limit($pagination->limit)->all();
    }

    public function getPagination(): Pagination
    {
        return new Pagination(['totalCount'=> $this->getQueryCommit()->count(), 'pageSize' => 5]);
    }

    private function getQueryCommit(): ActiveQuery
    {
        return self::find()->sort_desc()->noDeleted();
    }

    public function getDate(): string
    {
        return date('d-m-Y', $this->created_at);
    }

    public function isDeleted(): void
    {
        $this->is_deleted = 1;
        $this->save(false);
    }
    /**
     * Gets query for [[User]].
     */
    public function getCreated(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    public function getUpdated(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return CommitQuery the active query used by this AR class.
     */
    public static function find(): CommitQuery
    {
        return new CommitQuery(get_called_class());
    }
}
