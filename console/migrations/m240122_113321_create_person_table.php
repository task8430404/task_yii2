<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%person}}`.
 */
class m240122_113321_create_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // https://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $tableName = Yii::$app->db->tablePrefix . 'person';
        if (!(Yii::$app->db->getTableSchema($tableName, true) === null)) {
            $this->dropTable('person');
        }

        $this->createTable('{{%person}}', [
            'id' => $this->primaryKey(),
            'last_name' => $this->string(30)->notNull(),
            'first_name' => $this->string(30)->notNull(),
            'birthday' => $this->string(10)->notNull(),
            'phone' => $this->string(30)->null(),
            'gender' => $this->tinyInteger(1)->null(),
            'avatar' => $this->string(255)->null(),
            'user_id' => $this->integer(11)->notNull(),

            'status' => $this->tinyInteger(1)->defaultValue(1),
            'is_deleted' => $this->tinyInteger(1)->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull()->defaultValue(0),
            'updated_by' => $this->integer()->notNull()->defaultValue(0),

        ], $tableOptions);

        $this->addForeignKey(
            'personAndUserRelation_Fk',
            'person',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%person}}');
    }
}
