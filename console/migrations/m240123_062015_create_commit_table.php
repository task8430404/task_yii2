<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%commit}}`.
 */
class m240123_062015_create_commit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): void
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // https://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $tableName = Yii::$app->db->tablePrefix . 'commit';
        if (!(Yii::$app->db->getTableSchema($tableName, true) === null)) {
            $this->dropTable('commit');
        }

        $this->createTable('{{%commit}}', [
            'id' => $this->primaryKey(),
            'description' => $this->string(255)->notNull(),

            'status' => $this->tinyInteger(1)->defaultValue(1),
            'is_deleted' => $this->tinyInteger(1)->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull()->defaultValue(0),
            'updated_by' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->addForeignKey(
            'commitAndUserRelation_Fk',
            'commit',
            'created_by',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): void
    {
        $this->dropTable('{{%commit}}');
    }
}
